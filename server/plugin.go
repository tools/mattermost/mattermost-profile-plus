package main

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"strings"

	"github.com/mattermost/mattermost/server/public/plugin"
)

// ProfilePlusPlugin implements the interface expected by the Mattermost server
// to communicate between the server and plugin processes.
type ProfilePlusPlugin struct {
	plugin.MattermostPlugin
}

// Badge contains both the title and the color that represents a user badge
type Badge struct {
	Title string `json:"title"`
	Color string `json:"color"`
}

// Profile contains a set of attributes that describe the user
type Profile struct {
	Status string  `json:"status"`
	Badges []Badge `json:"badges"`
	Url    string  `json:"url"`
}

func (p *ProfilePlusPlugin) ServeHTTP(c *plugin.Context, w http.ResponseWriter, r *http.Request) {
	username := strings.TrimPrefix(r.URL.Path, "/username/")
	if username == "" {
		http.NotFound(w, r)
		return
	}
	if r.Header.Get("Mattermost-User-Id") == "" {
		http.Error(w, "You are not authorized to access this page", http.StatusUnauthorized)
		return
	}

	// Load plugin configuration (API URL template and headers)
	plugin := p.API.GetConfig().PluginSettings.Plugins["com.collabora.profile-plus"]
	apiURLTemplate := fmt.Sprintf("%v", plugin["api_url_template"])
	apiHTTPHeaders := fmt.Sprintf("%v", plugin["api_http_headers"])
	profileBaseUrl := fmt.Sprintf("%v", plugin["profile_base_url"])

	url := strings.Replace(apiURLTemplate, "{username}", username, 1)
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		http.Error(w, "Error creating request to external API", http.StatusInternalServerError)
		return
	}

	// Add the headers to the request
	var headers map[string]string
	if err := json.Unmarshal([]byte(apiHTTPHeaders), &headers); err != nil {
		http.Error(w, "Error parsing API headers", http.StatusInternalServerError)
		return
	}
	for k, v := range headers {
		req.Header.Add(k, v)
	}

	// Fetch the external API
	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		http.Error(w, "Error making a request to external API", http.StatusInternalServerError)
		return
	}
	defer resp.Body.Close()

	// Handle external API HTTP errors
	if resp.StatusCode != http.StatusOK {
		body, _ := io.ReadAll(resp.Body)
		http.Error(w, string(body), resp.StatusCode)
		return
	}

	// Parse the external API response
	var profile Profile
	if err := json.NewDecoder(resp.Body).Decode(&profile); err != nil {
		http.Error(w, "Error decoding the profile from JSON", http.StatusInternalServerError)
		return
	}
	profile.Url = profileBaseUrl

	// Prepare the plugin response
	content, err := json.Marshal(profile)
	if err != nil {
		http.Error(w, "Error encoding the profile to JSON", http.StatusInternalServerError)
		return
	}
	w.Header().Set("Content-Type", "application/json")
	w.Write(content)
}

func main() {
	plugin.ClientMain(&ProfilePlusPlugin{})
}
