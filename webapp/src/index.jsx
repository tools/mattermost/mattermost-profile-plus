// Copyright 2021, Collabora Ltd.
// SPDX-License-Identifier: LGPL-3.0-or-later

import ProfilePlus from "./components/ProfilePlus";
class ProfilePlusPlugin {
  async initialize(registry, store) {
    registry.registerPopoverUserAttributesComponent(ProfilePlus);
  }
}

window.registerPlugin("com.collabora.profile-plus", new ProfilePlusPlugin());
