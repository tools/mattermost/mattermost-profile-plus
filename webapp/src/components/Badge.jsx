// Copyright 2021, Collabora Ltd.
// SPDX-License-Identifier: LGPL-3.0-or-later

function Badge(props) {
  const style = {
    color: "#ffffff",
    backgroundColor: props.color,
    display: "inline-block",
    marginTop: "4px",
    padding: "3px 5px",
    borderRadius: "4px",
    fontWeight: "bold",
    fontSize: "0.8em",
  };
  return <span style={style}>{props.title}</span>;
}

export default Badge;
